package com.example.hackernews.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class News(
    val title: String,
    val url: String,
    val time: Long,
    val id: String
): Parcelable