package com.example.hackernews.di

import android.content.Context
import androidx.room.Room
import com.example.hackernews.data.db.NewsDb
import com.example.hackernews.data.network.ApiRequest
import com.example.hackernews.data.network.NetworkRequest
import com.example.hackernews.data.network.RequestTimeCheck
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun providesApiRequest(): ApiRequest = NetworkRequest()

    @Provides
    @Singleton
    fun provideGson(): Gson = Gson()

    @Provides
    @Singleton
    fun providesNewsDb(@ApplicationContext app: Context): NewsDb =
        Room.databaseBuilder(app, NewsDb::class.java, "news_db")
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun providesRequestTimeCheck(@ApplicationContext app: Context): RequestTimeCheck =
        RequestTimeCheck(
            app.getSharedPreferences(
                RequestTimeCheck.REQUEST_TIME_FILE, Context.MODE_PRIVATE
            )
        )
}