package com.example.hackernews.ui.news

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.hackernews.data.network.Resource
import com.example.hackernews.model.News
import com.example.hackernews.repository.NewsRepository
import com.example.hackernews.util.networkBoundResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NewsViewModel @Inject constructor(
    private val repository: NewsRepository
) : ViewModel() {

    private val _news: MutableStateFlow<Resource<List<News>>?> = MutableStateFlow(null)
    val news: StateFlow<Resource<List<News>>?> = _news
    private val triggerChannel = Channel<Unit>()

    init {

        viewModelScope.launch {
            triggerChannel.consumeAsFlow().collect {
                displayNews()
            }
        }

        viewModelScope.launch(IO) {
            repository.getNewsList().catch { error ->
                _news.value = Resource.Error(error)
            }.collect {
                it.data?.let {
                    if (triggerChannel.isClosedForSend.not()) {
                        triggerChannel.send(Unit)
                        triggerChannel.close()
                    }
                    return@collect
                }
            }
        }
    }

    fun displayNews() {
        viewModelScope.launch(IO) {
            repository.getNewsDetails()
                .catch { er -> Log.v("error_details", er.localizedMessage ?: "err null") }
                .collect { result ->
                    val oldList = arrayListOf<News>()
                    _news.value?.data?.let { oldList.addAll(it) }
                    result.data?.let { news ->
                        oldList.addAll(news)
                        _news.value = Resource.Success(oldList.toList())
                    }
                }
        }
    }
}
