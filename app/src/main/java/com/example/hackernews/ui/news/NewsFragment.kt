package com.example.hackernews.ui.news

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hackernews.data.network.Resource
import com.example.hackernews.databinding.NewsFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect


@AndroidEntryPoint
class NewsFragment : Fragment() {

    private var _binding: NewsFragmentBinding? = null
    private val binding: NewsFragmentBinding get() = _binding!!
    private val viewModel: NewsViewModel by viewModels()
    private lateinit var newsAdapter: NewsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = NewsFragmentBinding.inflate(inflater, container, false)
        setupAdapter()
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.news.collect { result ->
                when (result) {
                    is Resource.Success -> newsAdapter.submitList(result.data)

                    is Resource.Error -> Log.v(
                        "result_data",
                        result.error?.localizedMessage.toString()
                    )
                    is Resource.Loading -> Unit
                    else -> Unit
                }
            }
        }
        return binding.root
    }

    private fun setupAdapter() {
        newsAdapter = NewsAdapter(onItemClick = {
            findNavController().navigate(
                NewsFragmentDirections.actionNewsToNewsDetails(it.url)
            )
        }, reachedEnd = {
            viewModel.displayNews()
        })

        with(binding.newsRv) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = newsAdapter
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}