package com.example.hackernews.ui.news

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.hackernews.databinding.ItemNewsBinding
import com.example.hackernews.model.News
import com.example.hackernews.util.toDateString

class NewsAdapter(
    private val onItemClick: (news: News) -> Unit,
    private val reachedEnd: () -> Unit
) : ListAdapter<News, NewsAdapter.ViewHolder>(NewsComparator()) {


    inner class ViewHolder(private val binding: ItemNewsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            itemView.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    onItemClick(getItem(position))
                }
            }
        }

        fun bind(newItem: News) {
            with(binding) {
                tvTitle.text = newItem.title
                tvDate.text = newItem.time.toDateString()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemNewsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
        if (position == currentList.size - 1) {
            reachedEnd()
        }
    }
}

class NewsComparator : DiffUtil.ItemCallback<News>() {
    override fun areItemsTheSame(oldItem: News, newItem: News): Boolean = oldItem == newItem

    override fun areContentsTheSame(oldItem: News, newItem: News): Boolean = oldItem.id == newItem.id

}