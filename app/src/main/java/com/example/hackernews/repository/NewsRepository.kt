package com.example.hackernews.repository

import android.util.Log
import com.example.hackernews.data.db.NewsDb
import com.example.hackernews.data.db.entity.NewsIds
import com.example.hackernews.data.db.entity.NewsRecord
import com.example.hackernews.data.network.ApiRequest
import com.example.hackernews.data.network.RequestTimeCheck
import com.example.hackernews.data.network.Resource
import com.example.hackernews.model.News
import com.example.hackernews.util.Constants
import com.example.hackernews.util.networkBoundResource
import com.google.gson.Gson
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import org.json.JSONArray
import javax.inject.Inject

class NewsRepository @Inject constructor(
    private val apiRequest: ApiRequest,
    private val newsDb: NewsDb,
    private val gson: Gson,
    private val requestTimeCheck: RequestTimeCheck
) {

    private val newsDao = newsDb.newsDao()
    private val newsIdsDao = newsDb.newsIdsDao()
    private var lastIndex = 0

    fun getNewsList(): Flow<Resource<List<String>>> =
        networkBoundResource(
            query = {
                newsDao.getAllNewsIds()
            },
            fetch = {
                val result = apiRequest.get(url = Constants.NewsListUrl)
                val newsIds = JSONArray(result)
                val list = mutableListOf<String>()
                for (i in 0 until newsIds.length()) {
                    list.add(newsIds[i].toString())
                }
                Log.v("FETCH - list", list.toString())
                list
            },
            shouldFetch = { requestTimeCheck.shouldMakeRequest() },
            saveFetchResult = { ids ->
                ids.forEach { newsIdsDao.insertId(NewsIds(it)) }
            }
        )

    fun getNewsDetails(): Flow<Resource<List<News>>> = networkBoundResource(
        query = {
            flowOf(newsDao.getNews().map { it.mapToNews() })
        },
        fetch = {
            val ids = newsIdsDao.getIds()
            if (lastIndex >= ids.size) return@networkBoundResource emptyList<News>()
            val nextIndex =
                if (ids.size >= lastIndex + 10) lastIndex + 10 else ids.size
            val newsList = arrayListOf<News>()

            for (i in lastIndex + 1 until nextIndex) {
                val result = apiRequest.get(Constants.NewsDetailsUrl.format(ids[i]))
                newsList.add(gson.fromJson(result, News::class.java))
            }
            lastIndex = nextIndex
            newsList
        },
        saveFetchResult = { news: List<News> ->
            news.forEach { n ->
                newsDao.insert(NewsRecord(n.id, n.title, n.url, n.time))
            }
        },
        shouldFetch = { true }
    )
}