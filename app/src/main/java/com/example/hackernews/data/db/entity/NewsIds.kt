package com.example.hackernews.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "news_ids")
data class NewsIds(@PrimaryKey val id: String)