package com.example.hackernews.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.hackernews.data.db.entity.NewsRecord
import kotlinx.coroutines.flow.Flow

@Dao
interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(news: NewsRecord)

    @Query("SELECT * FROM news")
    suspend fun getNews(): List<NewsRecord>

    @Query("SELECT * FROM news WHERE id = :newsId")
    suspend fun getNewsById(newsId: String): NewsRecord

    @Query("SELECT EXISTS (SELECT 1 FROM news WHERE id = :newsId)")
    suspend fun checkNewsById(newsId: String): Boolean

    @Query("SELECT id from news")
    fun getAllNewsIds(): Flow<List<String>>
}