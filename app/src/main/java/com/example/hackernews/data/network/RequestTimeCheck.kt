package com.example.hackernews.data.network

import android.content.SharedPreferences

class RequestTimeCheck(private val prefs: SharedPreferences) {

    fun shouldMakeRequest(): Boolean {
        val lastRequestTIme = prefs.getLong(LAST_REQUEST_TIME, 0)
        val now = System.currentTimeMillis()
        if (now - lastRequestTIme >= REQUEST_WAIT_TIME) {
            prefs.edit().putLong(LAST_REQUEST_TIME, now).apply()
            return true
        }
        return false
    }

    companion object {
        private const val LAST_REQUEST_TIME = "Last_Request_Time"
        const val REQUEST_TIME_FILE = "Request_Time_File"
        private const val REQUEST_WAIT_TIME = 5 * 60 * 1000
    }
}