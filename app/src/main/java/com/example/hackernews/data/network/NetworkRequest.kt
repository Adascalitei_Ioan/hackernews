package com.example.hackernews.data.network

import com.example.hackernews.util.utf8
import kotlinx.coroutines.suspendCancellableCoroutine
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Reader
import java.net.HttpURLConnection
import java.net.URL
import kotlin.coroutines.resumeWithException


class NetworkRequest : ApiRequest {

    override suspend fun get(
        url: String,
        params: Map<String, String>?
    ): String {
        return suspendCancellableCoroutine { continuation ->
            try {
                val reader: BufferedReader

                var endpointPath = url
                params?.let { parameters ->
                    endpointPath = "$url?" + parameters.map { (k, v) -> "${k.utf8()}=${v.utf8()}" }
                        .joinToString("&")
                }
                val endpoint = URL(endpointPath)

                with(endpoint.openConnection() as HttpURLConnection) {
                    requestMethod = "GET"

                    reader = BufferedReader(InputStreamReader(inputStream) as Reader?)

                    val response = StringBuffer()
                    var inputLine = reader.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = reader.readLine()
                    }
                    reader.close()
                    if (continuation.isActive) {
                        continuation.resume(response.toString(), null)
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
                if (continuation.isActive) {
                    continuation.resumeWithException(e)
                }
            }
        }
    }
}