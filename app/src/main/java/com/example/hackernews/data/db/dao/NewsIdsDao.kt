package com.example.hackernews.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.hackernews.data.db.entity.NewsIds

@Dao
interface NewsIdsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertId(id: NewsIds)

    @Query("SELECT * FROM news_ids")
    suspend fun getIds(): List<String>
}