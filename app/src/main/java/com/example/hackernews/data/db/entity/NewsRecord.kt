package com.example.hackernews.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.hackernews.model.News

@Entity(tableName = "news")
data class NewsRecord(
    @PrimaryKey
    val id: String,
    val title: String,
    val url: String,
    val time: Long
) {
    fun mapToNews(): News = News(title, url, time, id)
}