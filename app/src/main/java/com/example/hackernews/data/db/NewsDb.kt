package com.example.hackernews.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.hackernews.data.db.dao.NewsDao
import com.example.hackernews.data.db.dao.NewsIdsDao
import com.example.hackernews.data.db.entity.NewsIds
import com.example.hackernews.data.db.entity.NewsRecord

@Database(entities = [NewsRecord::class, NewsIds::class], version = 1, exportSchema = false)
abstract class NewsDb : RoomDatabase() {
    abstract fun newsDao(): NewsDao
    abstract fun newsIdsDao(): NewsIdsDao
}