package com.example.hackernews.data.network

interface ApiRequest {

    suspend fun get(
        url: String,
        params: Map<String, String>? = null
    ) : String
}