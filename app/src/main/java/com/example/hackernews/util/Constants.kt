package com.example.hackernews.util

object Constants {

    const val NewsListUrl = "https://hacker-news.firebaseio.com/v0/topstories.json"
    const val NewsDetailsUrl = "https://hacker-news.firebaseio.com/v0/item/%s.json"
}