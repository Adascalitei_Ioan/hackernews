package com.example.hackernews.util

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
fun Long.toDateString(): String {
    val date = Date(this * 1000)
    val sdf= SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.getDefault())
    return sdf.format(date)
}

fun String.utf8(): String = java.net.URLEncoder.encode(this, "UTF-8")